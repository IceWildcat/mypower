// mypower.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <cstdio>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>
#include <vector>
#include <fstream>





std::string exec(const char* cmd) {
	std::array<char, 128> buffer;
	std::string result;
	std::shared_ptr<FILE> pipe(_popen(cmd, "r"), _pclose);
	if (!pipe) throw std::runtime_error("popen() failed!");
	while (!feof(pipe.get())) {
		if (fgets(buffer.data(), 128, pipe.get()) != nullptr)
			result += buffer.data();
	}
	return result;
}

void printVect(std::vector<std::string> vect) {
	for (unsigned int i = 0; i < vect.size(); i++) {
		std::cout << '[' << i << ']' << ' ' << vect[i] << '/' << std::endl;
	}
}

std::vector<std::string> parseString(std::string input) {
	
	//std::ofstream test;
	//test.open("dump.txt");
	//test << input.c_str();
	//test.close();



	std::vector<std::size_t> newlines;
	std::size_t current = input.find("\n");
	while (current != -1) {
		newlines.push_back(current);
		current = input.find("\n", current+2);
	}
	std::vector<std::string> ret;
	for (unsigned int i = 1; i < newlines.size();i++) {
		//std::cout << "[] " << input.substr(newlines[i - 1]+1, newlines[i] - newlines[i - 1]-1)<<std::endl;
		ret.push_back(input.substr(newlines[i - 1]+1, newlines[i] - newlines[i - 1]-1));
	}
	return ret;
}

class GUID;

std::ostream& operator<<(std::ostream& os, GUID& g);


class GUID {
private:
	std::string _name; //id
	std::string _title; //profile name

	
public:
	void setGUID(std::string name) {
		_name = name;
	}
	GUID() {
	}
	GUID(std::string name) {
		setGUID(name);
	}
	std::string getGUID() {
		return _name;
	}

	void setName(std::string title) {
		_title = title;
	}
	
	std::string getName() {
		return _title;
	}
};

class Powers { //TODO: add a lot of failsafes
	
private:
	std::vector<GUID> list;
	std::vector<std::string> names;
	int active; //index at list
public:
	void parseGUIDS(std::vector<std::string> GUIDcall) {
		list.erase(list.begin(), list.end());
		names.erase(names.begin(), names.end());
		for (unsigned int i = 2; i < GUIDcall.size();i++) { // skip first 2, title fillings.
			auto finpos = GUIDcall[i].find(std::string("  "));
			auto parenth = GUIDcall[i].find(std::string(")"));
			//size of guid is always 36
			list.push_back(GUID(GUIDcall[i].substr(finpos - 36, 36)));
			auto title = GUIDcall[i].substr(finpos + 3, parenth-finpos-3); //get name at the parenthesis
			names.push_back(title);
			list[i-2].setName(title);
			//std::cout << "[names] " << names[i-2] << std::endl;
			if (GUIDcall[i].find('*') != -1) { // it found the active
				active = i - 2;
			}
		}
	}

	friend std::ostream& operator<<(std::ostream& os, GUID& g);

	GUID getActive() {
		return list[active];
	}

	int getActiveIndex() {
		return active;
	}

	GUID getGUIDAt(int index) {
		return list[index];
	}

	std::string getNameAt(int index) {
		return names[index];
	}

	void printGUIDS() {
		for (unsigned int i = 0; i < list.size();i++) {
			std::cout << '[' << i << ']' << ' ' << list[i] << '/' << std::endl;
		}
	}

	void printCurrent() {
		auto ac = getActive();

		std::cout << ac.getGUID() << " (" << ac.getName() << ")" << std::endl;
	}

	GUID getByName(std::string name) {
		for (unsigned int i = 0; i < names.size(); i++) {
			if (names[i] == name) {
				return list[i];
			}
		}
		return GUID();
	}

	void setActive(int index) {

	}

	void setActive(GUID guid) {
		exec(((std::string)"powercfg /S " + guid.getGUID()).c_str());
		parseGUIDS(parseString(exec("powercfg /L")));
	}

};


std::ostream& operator<<(std::ostream& os, GUID& g) {
	os << g.getGUID();
	return os;
}



void printHelp() {
	std::cout << "Usage: " << std::endl
		<< "mypower.exe [ProfileName]" << std::endl << std::endl;
}


int main(int argc, char** argv)
{
	//TODO: handle commands

	std::vector<std::string> guidstr = parseString(exec("powercfg -list"));
	Powers cfg;
	cfg.parseGUIDS(guidstr);

	if (argc == 1) {
		std::cout << "Current: " << std::endl;
		cfg.printCurrent();
		exit(0);
	}
	else if (argc == 2) {
		if (argv[1] == "--help" || argv[1] == "-help" || argv[1] == "-?" || argv[1] == "/help" || argv[1] == "/?") {
			printHelp();
			exit(0);
		}
		else {
			GUID selection = cfg.getByName(std::string(argv[1]));
			if (selection.getName() != argv[1]) { // error
				std::cout << "No profile found by name: " << argv[1] << std::endl;
				exit(1);
			}
			else {
				cfg.setActive(selection);
				exit(0);
			}
		}
	}

	exit(1);


	



	//int* p = new int;
	//std::cin >> *p;

}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
